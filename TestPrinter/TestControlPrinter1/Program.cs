﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows;
using System.Management;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Text;
using iTextSharp.text;
using Path = System.IO.Path;
using System.Drawing.Imaging;
using Font = System.Drawing.Font;
using ceTe.DynamicPDF.Printing;
using System.Collections.Generic;

namespace TestControlPrinter1
{
    public class ModelParameter
    {
        public string typeName { get; set; }
        public List<byte[]> data { get; set; }
    }

    class PrintingExample
    {

        private Font printFont;
        private StreamReader streamToPrint;
        static string filePath = "C:\\ProjectAware\\Task\\Deploy_Project_KAsset.txt";

        public PrintingExample()
        {
            Printing();
        }

        // The PrintPage event is raised for each page to be printed.
        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            String line = null;

            // Calculate the number of lines per page.
            linesPerPage = ev.MarginBounds.Height /
               printFont.GetHeight(ev.Graphics);

            // Iterate over the file, printing each line.
            while (count < linesPerPage &&
               ((line = streamToPrint.ReadLine()) != null))
            {
                yPos = topMargin + (count * printFont.GetHeight(ev.Graphics));
                ev.Graphics.DrawString(line, printFont, Brushes.Black,
                   leftMargin, yPos, new StringFormat());
                count++;
            }

            // If more lines exist, print another page.
            if (line != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false;
        }

        // Print the file.
        public void Printing()
        {
            try
            {
                streamToPrint = new StreamReader(filePath);
                try
                {
                    printFont = new Font("Arial", 10);
                    PrintDocument pd = new PrintDocument();
                    pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
                    // Print the document.
                    pd.Print();
                }
                finally
                {
                    streamToPrint.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //MessageBox.Show(ex.Message);
            }
        }

        #region ExtractImagesFromPDF
        public static void ExtractImagesFromPDF(string sourcePdf, string outputPath)
        {
            // NOTE:  This will only get the first image it finds per page.
            PdfReader pdf = new PdfReader(sourcePdf);
            RandomAccessFileOrArray raf = new iTextSharp.text.pdf.RandomAccessFileOrArray(sourcePdf);

            try
            {
                for (int pageNumber = 1; pageNumber <= pdf.NumberOfPages; pageNumber++)
                {
                    PdfDictionary pg = pdf.GetPageN(pageNumber);
                    PdfDictionary res =
                      (PdfDictionary)PdfReader.GetPdfObject(pg.Get(PdfName.RESOURCES));
                    PdfDictionary xobj =
                      (PdfDictionary)PdfReader.GetPdfObject(res.Get(PdfName.XOBJECT));
                    if (xobj != null)
                    {
                        foreach (PdfName name in xobj.Keys)
                        {
                            PdfObject obj = xobj.Get(name);
                            if (obj.IsIndirect())
                            {
                                PdfDictionary tg = (PdfDictionary)PdfReader.GetPdfObject(obj);
                                PdfName type =
                                  (PdfName)PdfReader.GetPdfObject(tg.Get(PdfName.SUBTYPE));
                                if (PdfName.IMAGE.Equals(type))
                                {

                                    int XrefIndex = Convert.ToInt32(((PRIndirectReference)obj).Number.ToString(System.Globalization.CultureInfo.InvariantCulture));
                                    PdfObject pdfObj = pdf.GetPdfObject(XrefIndex);
                                    PdfStream pdfStrem = (PdfStream)pdfObj;
                                    byte[] bytes = PdfReader.GetStreamBytesRaw((PRStream)pdfStrem);
                                    if ((bytes != null))
                                    {
                                        using (MemoryStream memStream = new MemoryStream(bytes))
                                        {
                                            memStream.Position = 0;
                                            System.Drawing.Image img = System.Drawing.Image.FromStream(memStream);
                                            // must save the file while stream is open.
                                            if (!Directory.Exists(outputPath))
                                                Directory.CreateDirectory(outputPath);

                                            string path = Path.Combine(outputPath, String.Format(@"{0}.jpg", pageNumber));
                                            System.Drawing.Imaging.EncoderParameters parms = new System.Drawing.Imaging.EncoderParameters(1);
                                            parms.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Compression, 0);
                                            // GetImageEncoder is found below this method
                                            System.Drawing.Imaging.ImageCodecInfo jpegEncoder = GetImageEncoder("JPEG");
                                            img.Save(path, jpegEncoder, parms);
                                            break;

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            catch
            {
                throw;
            }
            finally
            {
                pdf.Close();
            }


        }
#endregion

        #region GetImageEncoder
        public static System.Drawing.Imaging.ImageCodecInfo GetImageEncoder(string imageType)
        {
            imageType = imageType.ToUpperInvariant();

            foreach (ImageCodecInfo info in ImageCodecInfo.GetImageEncoders())
            {
                if (info.FormatDescription == imageType)
                {
                    return info;
                }
            }

            return null;
        }
        #endregion

        private static PdfObject FindImageInPDFDictionary(PdfDictionary pg)
    {
        PdfDictionary res =
            (PdfDictionary)PdfReader.GetPdfObject(pg.Get(PdfName.RESOURCES));


        PdfDictionary xobj =
          (PdfDictionary)PdfReader.GetPdfObject(res.Get(PdfName.XOBJECT));
        if (xobj != null)
        {
            foreach (PdfName name in xobj.Keys)
            {

                PdfObject obj = xobj.Get(name);
                if (obj.IsIndirect())
                {
                    PdfDictionary tg = (PdfDictionary)PdfReader.GetPdfObject(obj);

                    PdfName type =
                      (PdfName)PdfReader.GetPdfObject(tg.Get(PdfName.SUBTYPE));

                    //image at the root of the pdf
                    if (PdfName.IMAGE.Equals(type))
                    {
                        return obj;
                    }// image inside a form
                    else if (PdfName.FORM.Equals(type))
                    {
                        return FindImageInPDFDictionary(tg);
                    } //image inside a group
                    else if (PdfName.GROUP.Equals(type))
                    {
                        return FindImageInPDFDictionary(tg);
                    }

                }
            }
        }

        return null;

    }
        static void Main(string[] args)
        {
            byte[] pdfBytes = System.IO.File.ReadAllBytes(@"C:\ProjectAware\Task\PrintingpageTest.pdf");
            PdfReader pdfReader = new PdfReader(pdfBytes);            
            int numberOfPages = pdfReader.NumberOfPages;
            InputPdf pdf = new InputPdf(pdfBytes);
            //PrintJob printJob = new PrintJob(Printer.Default, pdf);
            var ss = pdfReader.GetPageN(1).GetAsArray(PdfName.NOTE);
            Printer printerObj = new Printer("RICOH MPC2004ex PCL");
            PrintJob printJob = new PrintJob(printerObj, pdf);
            printJob.DocumentName = "PrintChequeExist";
            //PrintJob printJobObj = new PrintJob(printerObj, pdf);
            MemoryStream outputStream = new MemoryStream();
            PdfStamper stamper = new PdfStamper(pdfReader,outputStream);
            var type = "web-based | standalone | cloud-based";
            stamper.AcroFields.SetField("softwaretype", type);
            //Phrase header = new Phrase("Copy", new Font(FontFamily.HELVETICA, 14));
            for (var pageNum = 1; pageNum <= numberOfPages; pageNum++)
            {
                float x = pdfReader.GetPageSize(pageNum).GetBottom(20) / 2;
                float y = pdfReader.GetPageSize(pageNum).GetTop(20);
                // Get the page content and tokenize it.
                var contentBytes = pdfReader.GetPageContent(pageNum);
                var tokenizer = new PRTokeniser(new RandomAccessFileOrArray(contentBytes));

                var stringsList = new List<string>();
                while (tokenizer.NextToken())
                {
                    if (tokenizer.TokenType == PRTokeniser.TokType.OTHER)
                    {
                        String key = tokenizer.StringValue;
                        // Extract string tokens.
                        stringsList.Add(key);
                    }
                }

                // Print the set of string tokens, one on each line.
                Console.WriteLine(string.Join("\r\n", stringsList));
                //Setting paper source for whole print job. 
                //printJob.PrintOptions.PaperSource = printerObj.PaperSources[i];

                //Setting specific tray as paper source for first page in the print job. 
                PrintJobPage page = printJob.Pages[pageNum];
                page.PrintOptions.Inherit = false;
                if (pageNum == 1)
                {
                    for(int j=0;j< printerObj.PaperSources.Count; j++)
                    {
                        if(printerObj.PaperSources[j].Name.ToUpper() == "TRAY 1".ToUpper())
                        {
                            page.PrintOptions.PaperSource = printerObj.PaperSources[j];
                            break;
                        }
                        
                    }
                   

                }
                else if (pageNum == 2)
                {
                    for (int j = 0; j < printerObj.PaperSources.Count; j++)
                    {
                        if (printerObj.PaperSources[j].Name.ToUpper() == "TRAY 2".ToUpper())
                        {
                            page.PrintOptions.PaperSource = printerObj.PaperSources[j];
                            break;
                        }

                    }
                    //printJob.Dispose();
                    //page.PrintOptions.PaperSource = printerObj.PaperSources[0];
                }
                else
                {
                    printJob.Dispose();
                    //page.PrintOptions.PaperSource = printerObj.PaperSources[0];
                }
               

                //Setting specific tray as paper source for second page in the print job. 
                //PrintJobPage page2 = printJobObj.Pages[1];
                //page2.PrintOptions.Inherit = false;
                //page2.PrintOptions.PaperSource = printerObj.PaperSources[3];
            }

            printJob.Print();
            printJob.Dispose();

            //string sampleName = Environment.GetCommandLineArgs()[0];
            //if (args.Length != 1)
            //{
            //    Console.WriteLine("Usage: " + sampleName + " <file path>");
            //    return;
            //}
            //filePath = args[0];
            //new PrintingExample();

            //string ppath = @"C:\ProjectAware\Task\PrintingpageTest.pdf";
            //string outPutPath = @"C:\ProjectAware\Task\";
            //ExtractImagesFromPDF(ppath, outPutPath);

            //PdfReader pdfReader = new PdfReader(ppath);
            //int numberOfPages = pdfReader.NumberOfPages;
            //string[] words;
            //string line;
            ////string text;

            //for (int i = 1; i <= numberOfPages; i++)
            //{
            //    var text = PdfTextExtractor.GetTextFromPage(pdfReader, i, new LocationTextExtractionStrategy());

            //    words = text.Split('\n');
            //    for (int j = 0, len = words.Length; j < len; j++)
            //    {
            //        line = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(words[j]));
            //        Console.WriteLine(line);
            //    }
            //}
            //Console.WriteLine(numberOfPages);
            //Console.ReadLine();

            //string printerName = "RICOH MPC2004ex PCL";
            //string query = string.Format("SELECT * from Win32_Printer WHERE Name LIKE '%{0}'", printerName);

            //using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(query))
            //using (ManagementObjectCollection coll = searcher.Get())
            //{
            //    try
            //    {
            //        foreach (ManagementObject printer in coll)
            //        {
            //            foreach (PropertyData property in printer.Properties)
            //            {
            //                Console.WriteLine(string.Format("{0}: {1}", property.Name, property.Value));
            //            }
            //        }
            //    }
            //    catch (ManagementException ex)
            //    {
            //        Console.WriteLine(ex.Message);
            //    }
            //}
        }
    }
}
