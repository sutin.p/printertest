﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAppTestPrinter1
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        private System.ComponentModel.Container components;
        private System.Windows.Forms.Button printButton;
        private Font printFont;
        private StreamReader streamToPrint;
       
        string _paperName = "8x17"; // Printer paper name
        private Label label1;
        private static int counter = 0;
        private PrintDocument pd = null;
        int pageNum = 1;
        string _paperSource = "TRAY 2";
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //File.WriteAllText(@"C:\ProjectAware\Task\testfile.txt", "สุทิน พูนกิ่ง Hello World\r\nline2\r\nline3", Encoding.GetEncoding("windows-874"));
                //label1.Text = File.WriteAllText(@"c:\temp\testfile.txt", "Hello World\r\nline2\r\nline3", Encoding.Default);
                 // Printer Tray
                //PrintDocument printDoc = new PrintDocument();
                //PrinterSettings printSettings = printDoc.PrinterSettings;
                //string sTmp = "";

                //for (int i = 0; i < printSettings.PaperSources.Count; i++)
                //{
                //    sTmp += printSettings.PaperSources[i].SourceName;
                //    sTmp += " : ";
                //    sTmp += printSettings.PaperSources[i].Kind.ToString();
                //    sTmp += " : ";
                //    sTmp += printSettings.PaperSources[i].RawKind.ToString();
                //    sTmp += "\n";
                //}

                //MessageBox.Show(sTmp, "Paper Sources for " + printSettings.PrinterName + " : " +
                //                    printSettings.IsDefaultPrinter.ToString());
                streamToPrint = new StreamReader("C:\\ProjectAware\\Task\\Deploy_Project_KAsset.txt", Encoding.UTF8);

                try
                {
                    printFont = new Font("Arial", 10);
                    pd = new PrintDocument();
                    
                    //PaperSize oPS = new PaperSize();
                    //oPS.RawKind = (int)PaperKind.A4;
                    //PaperSource oPSource = new PaperSource();
                    //oPSource.RawKind = (int)PaperSourceKind.Upper;

                    //pd.PrinterSettings = new PrinterSettings();
                    //pd.PrinterSettings.PrinterName = "RICOH MPC2004ex PCL";
                    //pd.DefaultPageSettings.PaperSize = oPS;
                    //pd.DefaultPageSettings.Landscape = true;
                    //pd.DefaultPageSettings.PaperSource = oPSource;

                    //comboPaperSource.DisplayMember = "SourceName";

                    //PaperSource pkSource = new PaperSource();
                    //for (int i = 0; i < pd.PrinterSettings.PaperSources.Count; i++)
                    //{
                    //    if(pd.PrinterSettings.PaperSources[i].SourceName.ToUpper() == _paperSource.ToUpper())
                    //    {
                    //        pkSource = pd.PrinterSettings.PaperSources[i];
                    //        //break;
                    //    }
                        
                    //    //comboPaperSource.Items.Add(pkSource);
                    //}
                    
                   

                    //foreach (PaperSource _pSource in pd.PrinterSettings.PaperSources)
                    //{
                    //    //_pSource.SourceName = _paperSource.ToUpper();
                    //    if (_pSource.SourceName.ToUpper() == _paperSource.ToUpper())
                    //    {
                    //        pd.DefaultPageSettings.PaperSource = _pSource;
                    //        //pSource = _pSource; //Tested code :)
                    //        break;
                    //    }
                    //}

                    //pd.DefaultPageSettings.PaperSource = pd.PrinterSettings.PaperSources["Tray 2"];

                    //foreach (PaperSize _pSize in pd.PrinterSettings.PaperSizes)
                    //{
                    //    if (_pSize.PaperName.ToUpper() == _paperName.ToUpper())
                    //    {
                    //        pd.DefaultPageSettings.PaperSize = _pSize;
                    //        //pSize = _pSize; //Tested code :)
                    //        break;
                    //    }
                    //}

                    pd.PrinterSettings = new PrinterSettings();
                    pd.PrinterSettings.PrinterName = "RICOH MPC2004ex PCL";
                    //pd.DefaultPageSettings.PaperSource = pkSource;

                    pd.PrintPage += new PrintPageEventHandler(this.printDoc_PrintPage);
                    pd.QueryPageSettings += new QueryPageSettingsEventHandler(printDoc_QueryPageSettings);
                    //var pageNumber = 0;

                    //pd.PrintPage += (sender, e) =>
                    //{
                    //    PrintPage(pageNumber, text1, text2, imagePages[pageNumber], e);
                    //    if (pageNumber < imagePages.Count())
                    //    {
                    //        pageNumber++;
                    //        args.HasMorePages = pageNumber != imagePages.Count();
                    //    }
                    //};

                    pd.Print();
                    pd.Dispose();
                   
                }
                finally
                {
                    streamToPrint.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void printDoc_QueryPageSettings(object sender, QueryPageSettingsEventArgs e)
        {
            switch (pageNum)
            {
                case 1:
                    for (int i = 0; i < pd.PrinterSettings.PaperSources.Count; i++)
                    {
                        if (pd.PrinterSettings.PaperSources[i].SourceName.ToUpper() == _paperSource.ToUpper())
                        {
                            e.PageSettings.PaperSource = pd.PrinterSettings.PaperSources[i];
                            break;
                        }
                        
                    }
                    //e.PageSettings.PaperSource = pd.PrinterSettings.PaperSources[3];
                    break;
                case 2:
                    for (int i = 0; i < pd.PrinterSettings.PaperSources.Count; i++)
                    {
                        if (pd.PrinterSettings.PaperSources[i].SourceName.ToUpper() == "TRAY 1".ToUpper())
                        {
                            e.PageSettings.PaperSource = pd.PrinterSettings.PaperSources[i];
                            break;
                        }

                    }
                    //e.PageSettings.PaperSource = pd.PrinterSettings.PaperSources[5];
                    break;
            }
        }

        void printDoc_PrintPage(object sender, PrintPageEventArgs e)
        {
            Console.WriteLine("PageNum:{0} Papersource:{1}", pageNum, e.PageSettings.PaperSource);
            int count = 0;
            float linesPerPage = 0;
            float yPos = 0;
            float leftMargin = e.MarginBounds.Left;
            float topMargin = e.MarginBounds.Top;
            string line = null;
            linesPerPage = e.MarginBounds.Height /
               printFont.GetHeight(e.Graphics);

            // Print each line of the file.
            while (count < linesPerPage &&
               ((line = streamToPrint.ReadLine()) != null))
            {
                yPos = topMargin + (count * printFont.GetHeight(e.Graphics));
                e.Graphics.DrawString("This should go tray1" + line, printFont, Brushes.Black, leftMargin, yPos, new StringFormat());
                Console.WriteLine(line);
                //e.Graphics.DrawString("This should go tray1", printFont, Brushes.Black, 10, 10);
                count++;
            }
            //e.Graphics.DrawString("This should go tray1", printFont, Brushes.Black, 10, 10);
            //e.HasMorePages = true;
            if (line != null)
            {
                e.HasMorePages = true;
                pageNum++;
            }
            else
            {
                e.HasMorePages = false;
            }
            //if (pageNum == 1)
            //{
            //    // Print each line of the file.
            //    while (count < linesPerPage &&
            //       ((line = streamToPrint.ReadLine()) != null))
            //    {
            //        yPos = topMargin + (count * printFont.GetHeight(e.Graphics));
            //        e.Graphics.DrawString("This should go tray1" + line, printFont, Brushes.Black, leftMargin, yPos, new StringFormat());
            //        //e.Graphics.DrawString("This should go tray1", printFont, Brushes.Black, 10, 10);
            //        count++;
            //    }
            //    //e.Graphics.DrawString("This should go tray1", printFont, Brushes.Black, 10, 10);
            //    //e.HasMorePages = true;
            //    if (line != null)
            //    {
            //        e.HasMorePages = true;
            //        pageNum++;
            //    }
            //    else
            //    {
            //        e.HasMorePages = false;
            //    }

            //}
            //if (pageNum == 2)
            //{
            //    while (count < linesPerPage &&
            //      ((line = streamToPrint.ReadLine()) != null))
            //    {
            //        yPos = topMargin + (count * printFont.GetHeight(e.Graphics));
            //        e.Graphics.DrawString("This should go tray1" + line, printFont, Brushes.Black, leftMargin, yPos, new StringFormat());
            //        //e.Graphics.DrawString("This should go tray1", printFont, Brushes.Black, 10, 10);
            //        count++;
            //    }
            //    //e.Graphics.DrawString("This should go tray3", printFont, Brushes.Black, 10, 10);
            //}

        }

        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            Graphics graphic = e.Graphics;
            SolidBrush brush = new SolidBrush(Color.Black);

            Font font = new Font("Courier New", 12);

            e.PageSettings.PaperSize = new PaperSize("A4", 850, 1100);

            float pageWidth = e.PageSettings.PrintableArea.Width;
            float pageHeight = e.PageSettings.PrintableArea.Height;

            float fontHeight = font.GetHeight();
            int startX = 40;
            int startY = 30;
            int offsetY = 40;

            for (int i = 2; i < 100; i++)
            {
                graphic.DrawString("Line: " + i, font, brush, startX, startY + offsetY);
                offsetY += (int)fontHeight;

                if (offsetY >= pageHeight)
                {
                    e.HasMorePages = true;
                    offsetY = 0;
                    return;
                }
                else
                {
                    e.HasMorePages = false;
                }
            }
        }

        // The PrintPage event is raised for each page to be printed.
        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            string line = null;
            // Calculate the number of lines per page.
            linesPerPage = ev.MarginBounds.Height /
               printFont.GetHeight(ev.Graphics);

            // Print each line of the file.
            while (count < linesPerPage &&
               ((line = streamToPrint.ReadLine()) != null))
            {
                yPos = topMargin + (count * printFont.GetHeight(ev.Graphics));
                ev.Graphics.DrawString(line, printFont, Brushes.Black, leftMargin, yPos, new StringFormat());
                count++;
            }

            // If more lines exist, print another page.
            if (line != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false;
        }

       
        private static void PrintPage(string text1, string text2, byte[][] imageData, PrintPageEventArgs args)
        {

            foreach (var b in imageData)
            {
                using (var stream = new MemoryStream(b))
                {
                    var i = Image.FromStream(stream);

                    //CreateNotApprovedWatermark(i, text1, text2);

                    if (args.PageSettings.PrinterSettings.CanDuplex)
                    {
                        args.PageSettings.PrinterSettings.Duplex = Duplex.Horizontal;
                    }

                    var m = args.MarginBounds;

                    if (i.Width / (double)i.Height > m.Width / (double)m.Height) // image is wider
                    {
                        m.Height = (int)(i.Height / (double)i.Width * m.Width);
                    }
                    else
                    {
                        m.Width = (int)(i.Width / (double)i.Height * m.Height);
                    }
                    args.Graphics.DrawImage(i, m);
                    if (counter <= 2)
                    {
                        counter++;
                        args.HasMorePages = true;
                    }
                    else
                    {
                        args.HasMorePages = false;
                    }
                }
            }

        }

        //The Windows Forms Designer requires the following procedure.
        private void InitializeComponent()
        {
            this.printButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // printButton
            // 
            this.printButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.printButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.printButton.Location = new System.Drawing.Point(32, 110);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(136, 40);
            this.printButton.TabIndex = 0;
            this.printButton.Text = "Print the file.";
            this.printButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(504, 381);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.printButton);
            this.Name = "Form1";
            this.Text = "Print Example";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
